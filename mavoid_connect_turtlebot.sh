#!/bin/bash
./a.out
turtlebotIP=$(ifconfig | grep 'inet ' | grep -v '127.0.0.1' | sed -n -e 's/^.*inet //p' | sed -n -e 's/\s.*//p')
remoteIP=$(head -n 1 database.txt)
sed -i "s/ROS_HOSTNAME=.*/ROS_HOSTNAME=$turtlebotIP/" ~/.bashrc
sed -i "s/ROS_MASTER_URI=.*/ROS_MASTER_URI=http:\/\/$remoteIP:11311/" ~/.bashrc
