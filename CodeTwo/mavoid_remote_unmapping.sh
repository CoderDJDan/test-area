#!/usr/bin/env bash
mapPid=$(ps -aux | grep "methods:=gmapping" | grep -v grep | awk '{print $2}')
kill $mapPid
sleep 3
