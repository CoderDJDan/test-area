#!/usr/bin/env bash
roslaunch turtlebot3_bringup turtlebot3_robot.launch &>/dev/null &
sleep 15
roslaunch raspicam_node camera_module_v2_640x480_30fps.launch &>/dev/null &
sleep 15
rosservice call /raspicam_node/start_capture
sleep 15
