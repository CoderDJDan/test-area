#!/usr/bin/env bash
rqtPid=$(ps -aux | grep "rqt_image_view" | grep -v grep | awk '{print $2}')
kill $rqtPid
sleep 1
masterPid=$(ps -aux | grep "rosmaster" | grep -v grep | awk '{print $2}')
kill $masterPid
sleep 5
