#!/usr/bin/env bash
nodePid=$(ps -aux | grep " raspicam_node" | grep -v grep | awk '{print $2}')
kill $nodePid
sleep 1
launchPid=$(ps -aux | grep "bringup " | grep -v grep | awk '{print $2}')
kill $launchPid
sleep 10
