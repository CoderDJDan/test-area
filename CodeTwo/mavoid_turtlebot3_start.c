/***********************************************************************************
 *
 * Author: Dr. Lisa Frye
 * Creation Date: November 27, 2000
 * Modified Date: August 29, 2015
 * Filename: tcpserv.c
 * Purpose: Server example for TCP Sockets; reads one character at a time
 * Adapted from Haviland book
 *
 **********************************************************************************/ 

#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define SIZE sizeof(struct sockaddr_in)
//Port number to connect to server,  as set forth by RFC 865
#define PORTNUMBER 62864
//Number of bytes to send to/recieve from server
#define MESSAGESIZE 16
#define COMMANDSIZE 100


void catcher(int sig);
int newsockfd;


int main()
{
  int sockfd;
  char c;
  int pid;
  struct sockaddr_in server = {AF_INET, PORTNUMBER, INADDR_ANY};
  static struct sigaction act;

  act.sa_handler = catcher;
  sigfillset(&(act.sa_mask));
  sigaction(SIGPIPE, &act, NULL);

  server.sin_port = htons(PORTNUMBER);

  // set up the transport end point
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
      perror("socket call failed");
      exit(-1);
    }   // end if 

  // bind and address to the end point
  if (bind(sockfd, (struct sockaddr *)&server, SIZE) == -1)
    {
      perror("bind call failed");
      exit(-1);
    }   // end if bind

  // start listening for incoming connections
  if (listen(sockfd, 1) == -1)
    {
      perror("listen call failed");
      exit(-1);
    }   // end if

    int childStatus;
  for (;;)
    {
      // accept connection
      // if returns an EINTR (interrupted system call) then can re-call
      while (((newsockfd = accept(sockfd, NULL, NULL)) == -1) && (errno == EINTR));
		if (newsockfd == -1)
		{
		  perror("accept call failed");
		  exit(-1);
		}   // end if

		  // spawn a child to deal with the connection
		  if ((pid = fork()) == -1)
		{
		  perror("fork failed");
		  exit(-1);
		}   // end if

		  if (pid == 0)
		  {  
        char *ipAddress=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store confirmation message from server
        ipAddress[0] = '\0'; //Just in case, initialize string to be (effectively) empty

        if (recv(newsockfd, ipAddress, MESSAGESIZE, 0) <= 0) //If any message is sent by the client to the server
        {
          perror("Error recieving message from server");
          
          close(newsockfd); //Close socket
          free(ipAddress); //Free memory allocated to store the incoming quote
          exit(-1);
        }
          printf("Got IP\n");
        
        char *systemCommand=(char *)malloc(sizeof(char)*COMMANDSIZE); //Store confirmation message from server
        strcpy(systemCommand, "sed -i \"s|ROS_MASTER_URI=.*|ROS_MASTER_URI=http://");
        strcat(systemCommand,ipAddress);
        strcat(systemCommand,":11311|\" ~/.bashrc");
        system(systemCommand);
        strcpy(systemCommand, "source ~/.bashrc");
        system(systemCommand);
        
        if (send(newsockfd, "run roscore", MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
        {
          perror("Error sending message to server");
          
          close(newsockfd); //Close socket
          exit(-1);
        }
        printf("Sent Roscore\n");
			  free(ipAddress); //Free memory allocated to store the incoming quote

	      char *confirmMessage=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store confirmation message from server
	      confirmMessage[0] = '\0'; //Just in case, initialize string to be (effectively) empty
	      char matchingString[MESSAGESIZE] = "run launch";
        if (recv(newsockfd, confirmMessage, MESSAGESIZE, 0) > 0) //If any message is sent by the server to the client
        {
          if (strcmp(confirmMessage, matchingString) != 0)
          {
            perror("Error recieving message from server");
            close(newsockfd); //Close socket
            free(confirmMessage); //Free memory allocated to store the incoming quote
            exit(-1);
          }
          printf("Got launch\n");
          system("bash -i -c \"source ~/.bashrc && ./test-area/CodeTwo/mavoid_turtlebot3_bringup.sh\"");
        }
        else
        {
          perror("Error recieving message from server");
          
          close(newsockfd); //Close socket
          free(confirmMessage); //Free memory allocated to store the incoming quote
          exit(-1);
        }


        if (send(newsockfd, "complete", MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
        {
          perror("Error sending message to server");
          
          close(newsockfd); //Close socket
          exit(-1);
        }
          printf("Sent Complete\n");
        
	      strcpy(matchingString,"exit");
        if (recv(newsockfd, confirmMessage, MESSAGESIZE, 0) > 0) //If any message is sent by the server to the client
        {
          if (strcmp(confirmMessage, matchingString) != 0)
          {
            perror("Error recieving message from server");
            close(newsockfd); //Close socket
            free(confirmMessage); //Free memory allocated to store the incoming quote
            exit(-1);
          }
          printf("Recieved command to end things.\n");
          system("./test-area/CodeTwo/mavoid_turtlebot3_end.sh");
        }
        else
        {
          perror("Error recieving message from server");
          
          close(newsockfd); //Close socket
          free(confirmMessage); //Free memory allocated to store the incoming quote
          exit(-1);
        }
        
        if (send(newsockfd, "closing", MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
        {
          perror("Error sending message to server");
          
          close(newsockfd); //Close socket
          exit(-1);
        }
          printf("Sent Closing\n");

        close(newsockfd);
        exit(0);
	    }   // end child process
      // parent doesn't need newsockfd
      close(newsockfd);
      if (wait(&childStatus) == -1)
      {
          perror("Error in wait: ");
          exit(-1);
      }
      else
      {
          break;
      }
    }   // end for
}   // end main



void catcher(int sig)
{
  close(newsockfd);
  exit(0);
}   // end function catcher
