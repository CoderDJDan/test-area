#!/usr/bin/env bash
turtlebotIP=$(ifconfig | grep 'inet ' | grep -v '127.0.0.1' | sed -n -e 's/^.*inet //p' | sed -n -e 's/\s.*//p')
sed -i "s/ROS_HOSTNAME=.*/ROS_HOSTNAME=$turtlebotIP/" ~/.bashrc
./test-area/CodeTwo/mavoid_turtlebot3_start
