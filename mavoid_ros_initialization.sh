#!/bin/bash
cd ..
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update -y
sudo apt upgrade -y
sudo dpkg --configure -a
sudo apt install -y ros-noetic-desktop-full
source /opt/ros/noetic/setup.bash
if ! grep '/opt/ros/noetic/setup.bash' ~/.bashrc
then
    echo 'source /opt/ros/noetic/setup.bash' >> ~/.bashrc
fi
source ~/.bashrc
mkdir -p ~/ws/src
cd ~/ws/src/
sudo apt install -y git
git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3.git
git clone https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
cd ..
catkin_make
if ! grep 'source /home/$USER/ws/devel/setup.bash' ~/.bashrc
then
    echo 'source /home/$USER/ws/devel/setup.bash' >> ~/.bashrc
fi
if ! grep 'export TURTLEBOT3_MODEL=burger' ~/.bashrc
then
    echo 'export TURTLEBOT3_MODEL=burger' >> ~/.bashrc
fi
if ! grep 'export ROS_MASTER_URI=http://' ~/.bashrc
then
    echo 'export ROS_MASTER_URI=http://' >> ~/.bashrc
fi
if ! grep 'export ROS_HOSTNAME=' ~/.bashrc
then
    echo 'export ROS_HOSTNAME=' >> ~/.bashrc
fi
source ~/.bashrc
cd ..
sudo apt-get install -y ros-noetic-move-base ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers net-tools ros-noetic-map-server gnome-terminal
