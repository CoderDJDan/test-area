/*****************************************************************************/
/*  Author:            Bohdan D. Jacklitsch                                  */
/*  Creation Date:     11/5/2020                                             */
/*  Last Updated:      11/19/2020                                            */
/*  Due Date:          11/30/2020                                            */
/*  Course:            CSC355 - Software Engineering II                      */
/*  Professor Name:    Professor Donna Demarco                               */
/*  Filename:          mavoid_remote_start.c                                 */
/*  Language:          C                                                     */
/*  Compiler:          g++                                                   */
/*  Execution Command: ./mavoid_remote_start                                 */
/*  Purpose:           This program takes in arguments of the turtlebot3's   */
/*                     IP address and the Remote PC's IP address, and        */
/*                     acts as a client to a server running on the           */
/*                     turtlebot3. It then sends the server the Remote PC's  */
/*                     IP address so that the Robotis server can be created, */
/*                     boots up necessary ROS processes, and gives the user  */
/*                     a menu on whether they want to map, navigate, or exit.*/
/*****************************************************************************/
//TO DO
//Change from scanf in menu
//May be able to skip sending IP by having server read IP of client
//Server autostartup: determine if connected to internet with ping


//As usual, no clue what most of these do or if they are necessary
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

//Server's port number, a hopefully unused port number
#define PORTNUMBER 62864
//Maximum number of bytes in a protocol
#define MESSAGESIZE 16

/*  interpretServerAddress
    Description: takes the server's IP address and normalizes it, if necessary
    Parameters and Return:
        inputServer (in) - IP of server
        serverAddress (out) - Normalized server address
*/
void interpretServerAddress(char *inputServer, struct sockaddr_in **serverAddress);

/*  connectToServer
    Description: Creates a socket and connects to the server
    Parameters and Return:
        serverAddress (in) - Normalized server address
        return - Socket file descriptor
*/
int connectToServer(struct sockaddr_in **serverAddress);

/*  sendIPAddress
    Description: sends the client's IP address to the server
    Parameters and Return:
        socketDescriptor (in) - Socket file descriptor
        remoteIP (in) - Remote PC's IP Address
*/
void sendIPAddress(int socketDescriptor, char* remoteIP);

/*  initializeBothSystems
    Description: starts core ROS services on client and instructs
                 server to do the same
    Parameters and Return:
        socketDescriptor (in) - Socket file descriptor
*/
void initializeBothSystems(int socketDescriptor);

/*  mavoidMainMenu
    Description: brings up menu of choices to continue and loops
    Parameters and Return: None
*/
void mavoidMainMenu();

/*  optionStartMapping
    Description: tries to start gmapping
    Parameters and Return:
        inMap (in/out) - True if currently mapping
		inNavigate (in/out) - True if currently navigating
*/
void optionStartMapping(bool &inMap, bool &inNavigate);

/*  optionStartNavigation
    Description: tries to start navigation
    Parameters and Return:
        inMap (in/out) - True if currently mapping
		inNavigate (in/out) - True if currently navigating
*/
void optionStartNavigation(bool &inMap, bool &inNavigate);

/*  optionBackOut
    Description: Back out from either mapping or navigation
    Parameters and Return:
        inMap (in/out) - True if currently mapping
		inNavigate (in/out) - True if currently navigating
*/
void optionBackOut(bool &inMap, bool &inNavigate);

/*  saveBeforeExit
    Description: Asks the user if they want to save before exiting mapping,
	             or if thwy want to cancel exiting the map
    Parameters and Return:
		inMap (out) - True if user cancelled
*/
void saveBeforeExit(bool &inMap);

/*  printCommandList
    Description: Prints list of commands the user can enter
    Parameters and Return: None
*/
void printCommandList();

/*  optionGetHelp
    Description: Prints instructions/help menu
    Parameters and Return: None
*/
void optionGetHelp();

/*  endRosServices
    Description: End core ROS services on both turtlebot3 and Remote PC
    Parameters and Return:
		socketDescriptor (in) - Socket file descriptor
*/
void endRosServices(int socketDescriptor);

/*  main
    Arguments: 2
        argv[1] - IP address of server (turtlebot3)
        argv[2] - Remote PC's IP Address
*/
int main(int argc, char **argv)
{
	int socketDescriptor; //Socket file descriptor for the client
	struct sockaddr_in *serverAddress; //Address for the server
	
	interpretServerAddress(argv[1], &serverAddress); //Normalize turtlebot's address to server address
	
	connectToServer(&serverAddress); //Connect to turtlebot3's server
	sendIPAddress(socketDescriptor, argv[2]); //Send Remote PC IP to turtlebot3
    initializeBothSystems(socketDescriptor); //Start core services on both systems

    mavoidMainMenu(); //Main menu, user input location
	
	endRosServices(socketDescriptor);
	close(socketDescriptor); //Close socket
	
	return 0;
}

//Uses the passed server's IP address to make a normalized server address
void interpretServerAddress(char *inputServer, struct sockaddr_in **serverAddress)
{
	struct addrinfo *serverInfo = (struct addrinfo *)malloc(sizeof(struct addrinfo)); //Allocate memory for addrinfo for server details
	
	int returnHolder; //Stores return value of function/process for evaluation
	
	memset(serverInfo, 0, sizeof(serverInfo));
	serverInfo->ai_family = AF_INET; //As we are using an Internet Standard, seems reasonable that it would work with just the internet
	serverInfo->ai_flags = 0; //No flags that I am aware of
	serverInfo->ai_protocol = IPPROTO_TCP; //Might be redundant, but restricts protocol to only TCP
	serverInfo->ai_socktype = SOCK_STREAM; //To make the socket TCP
	
	returnHolder = getaddrinfo(inputServer, 0, serverInfo, &serverInfo);
	if (returnHolder < 0) //If error
	{
		printf("Error in getaddrinfo call: %s\n", gai_strerror(returnHolder));
		exit(-1);
	}
	
	*serverAddress = (struct sockaddr_in *)serverInfo->ai_addr; //Extract address
	(*serverAddress)->sin_port = htons(PORTNUMBER); //Make Port number to system's required port number
	
	free(serverInfo); //Free memory of addrinfo, as server info was interpreted
}

//Connects to the turtlebot3's server
int connectToServer(struct sockaddr_in **serverAddress)
{  
	int returnHolder; //Stores return value of function/process for evaluation
	int socketDescriptor; //Stores the socket file descriptor of the socket

	socketDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); //Create a socket for the client
	if (socketDescriptor < 0)
	{
		perror("Error creating socket: ");
		exit(-1);
	}
	
	returnHolder = connect(socketDescriptor, (struct sockaddr *) *serverAddress, sizeof(struct sockaddr)); //Connect to server
	if (returnHolder == -1) //If error
	{
		perror("Error connecting to server: ");
		exit(-1);
	}
    return (socketDescriptor);
}

//Sends over the IP address of the Remote PC
void sendIPAddress(int socketDescriptor, char* remoteIP)
{  
    printf("Attempting to send IP Address\n");
	if (send(socketDescriptor, remoteIP, MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
	{
		perror("Error sending message to server");
		
		close(socketDescriptor); //Close socket
		exit(-1);
	}
	printf("Sent IP address of Remote PC %s to turtlebot3\n", remoteIP);
	
	char *protocolMessage=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store confirmation message from server
	protocolMessage[0] = '\0'; //Just in case, initialize string to be (effectively) empty

	char matchingString[MESSAGESIZE] = "run roscore"; //Protocol signifying everything is setup for roscore now
  
	printf("Awaiting confirmation to continue\n");
	if (recv(socketDescriptor, protocolMessage, MESSAGESIZE, 0) > 0) //If any message is sent by the server to the client
	{
		if (strcmp(protocolMessage, matchingString) != 0)
		{
			printf("Received unexpected message from server");
			close(socketDescriptor); //Close socket
			free(protocolMessage); //Free memory allocated to store the incoming protocol
			exit(-1);
		}
	}
	else
	{
		perror("Error recieving message from server");
		
		close(socketDescriptor); //Close socket
		free(protocolMessage); //Free memory allocated to store the incoming protocol
		exit(-1);
	}
	printf("Receieved permission from turtlebot3\n");
}

//Starts up core ros systems and instructs turtlebot3 to do the same
void initializeBothSystems(int socketDescriptor)
{
	char *protocolMessage=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store protocol from server
	protocolMessage[0] = '\0'; //Just in case, initialize string to be (effectively) empty
    char matchingString[MESSAGESIZE];
    
    printf("Starting roscore and related services\n");
	system("bash -i -c \"source ~/.bashrc && ./sprint4/mavoid_remote_roscore.sh\""); //Script file to run roscore
    printf("Started roscore\n");
	
    printf("Sending instructions to start ros utilities on turtlbot3\n");
    strcpy(protocolMessage, "run launch"); //Protocol to signify that turtlebot3 can initialize
	if (send(socketDescriptor, protocolMessage, MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
	{
		perror("Error sending message to server");
		
		close(socketDescriptor); //Close socket
	    free(protocolMessage); //Free memory allocated to store the incoming quot
		exit(-1);
	}
    printf("Sent instructions\n");
  
    printf("Awaiting permission to continue\n");
    strcpy(matchingString, "complete"); //Protocol to signify that RemotePC can finish up initialization
	if (recv(socketDescriptor, protocolMessage, MESSAGESIZE, 0) > 0) //If any message is sent by the server to the client
	{
		if (strcmp(protocolMessage, matchingString) != 0)
		{
			perror("Error recieving message from server");
			close(socketDescriptor); //Close socket
			free(protocolMessage); //Free memory allocated to store the incoming protocol
			exit(-1);
		}
	}
	else
	{
		perror("Error recieving message from server");
		
		close(socketDescriptor); //Close socket
		free(protocolMessage); //Free memory allocated to store the incoming quote
		exit(-1);
	}
    printf("Got permission to continue startup\n");

    printf("Starting stream viewer\n");
    system("./sprint4/mavoid_remote_stream.sh"); //Script file to start stream viewer
    printf("Stream viewer started\n");

    printf("\n\nInitialization Complete\n\n\n\n");
}

//Try and start mapping
void optionStartMapping(bool &inMap, bool &inNavigate)
{
	if (inMap)
	{
		printf("You are already in mapping mode\n");
	}
	else if (inNavigate)
	{
		system("kill $(ps -aux | grep ' turtlebot3_navigation '  | grep -v grep | awk '{print $2}')"); //Stops navigation
		inNavigate = false;
		system("sleep 2");
		system("./sprint4/mavoid_remote_mapping.sh"); //Script to start mapping
		inMap = true;
	}
	else
	{
		system("./sprint4/mavoid_remote_mapping.sh"); //Script to start mapping
		inMap = true;
	}
}

//Try to start navigation
void optionStartNavigation(bool &inMap, bool &inNavigate)
{
	if (inNavigate)
	{
		printf("You are already in navigation mode\n");
	}
	else if (inMap)
	{
		saveBeforeExit(inMap); //Asks whether user wants to save map before exit or not, or cancel
		if (!inMap) //Meaning user selected cancel
		{
			system("./sprint4/mavoid_remote_navigate.sh"); //Script to start navigation
			inNavigate = true;
		}
	}
	else
	{
		system("./sprint4/mavoid_remote_navigate.sh"); //Script to start navigation
		inNavigate = true;
	}
}

//Ask user if they want to save before exit, just exit, or cancel their choice
void saveBeforeExit(bool &inMap)
{
	char *userOption=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store selection of user
	userOption[0] = '\0'; //Just in case, initialize string to be (effectively) empty
	bool gotConfirmation = false;

	printf("Would you like to save your map before exiting mapping? (y/n/c)\n");
	while (!gotConfirmation)
	{
		scanf("%s", userOption);
		if (strcmp(userOption, "y") == 0)
		{
			system("./sprint4/mavoid_remote_save.sh"); //Script to save the map
			gotConfirmation = true;
		}
		else if (strcmp(userOption, "n") == 0)
		{
			gotConfirmation = true;
		}
		else if (strcmp(userOption, "c") == 0)
		{
			return; //Return before stopping the map
		}
		else
		{
			printf("Sorry, input not understood. Enter \"y\" if you want to save, \"n\" if you do not, or \"c\" if you want to cancel.\n");
		}
	}
	system("kill $(ps -aux | grep 'methods:=gmapping' | grep -v grep | awk '{print $2}')"); //Exit mapping
	inMap = false;
	system("sleep 4"); //Wait to ensure mapping has closed
}

//Backs out of mapping or navigation
void optionBackOut(bool &inMap, bool &inNavigate)
{
	if (inNavigate)
	{
		system("kill $(ps -aux | grep ' turtlebot3_navigation ' | grep -v grep | awk '{print $2}')"); //Stops navigation
		system("sleep 2"); //Wait for it to fully stop
		inNavigate = false;
	}
	else if (inMap)
	{
		saveBeforeExit(inMap); //Asks whether user wants to save map before exit or not, or cancel
	}
	else
	{
		printf("There is nothing to back out of.\n");
	}
}

//Prints list of commands
void printCommandList()
{
    printf("List of commands:\n");
    printf("     map - Start a new map to move the turtlebot3 and map the surrounding area.\n");
    printf("     navigate - Open the last map saved to have the turtlebot3 navigate.\n");
    printf("     back - Goes back from map or navigate to menu.\n");
    printf("     help - Displays menu along with additional notes.\n");
    printf("     exit - End all ros services and disconnect from turtlebot3.\n");
}

//Prints help menu
void optionGetHelp()
{
	printf("Most commands for interacting opening and closing ROS applications will go in this terminal.\n");
	printf("Ensure user input is lowercase.\n");
	printf("Upon opening an application with this terminal, you can also close it with this terminal too.\n");
	printf("Some commands will open applications and give new options/instructions, as shown in this terminal.\n");
	printf("So basically, this terminal is your friend!\n");
    printCommandList();
}

//Brings up menu of options for user to select and continue
void mavoidMainMenu()
{
	char *userOption=(char *)malloc(sizeof(char)*MESSAGESIZE);  //Store selection of user
	userOption[0] = '\0'; //Just in case, initialize string to be (effectively) empty
    char matchingString[MESSAGESIZE];
	bool inMap = false;
	bool inNavigate = false;

    printf("Welcome to MAVOID's simple mapping and navigation user portal for turtlebot3!\n");
    printf("Use any of the following commands (must be lowercase) to interact with the turtlebot3.\n");
	printCommandList(); //Print command list
    
    while(true)
    {
        scanf("%s", userOption);

		if (strcmp(userOption, "map") == 0)
		{
			optionStartMapping(inMap, inNavigate); //Try and start save
		}
		else if (strcmp(userOption, "save") == 0)
		{
			if (inMap)
			{
				system("./sprint4/mavoid_remote_save.sh");
			}
			else
			{
				printf("Sorry, but you must be mapping in order to save the map\n");
			}
		}
		else if (strcmp(userOption, "navigate") == 0)
		{
			optionStartNavigation(inMap, inNavigate); //Try and start navigation
		}
		else if (strcmp(userOption, "back") == 0)
		{
			optionBackOut(inMap, inNavigate); //Back out of mapping or navigation
		}
		else if (strcmp(userOption, "help") == 0)
		{
			optionGetHelp(); //Print help menu
		}
		else if (strcmp(userOption, "exit") == 0)
		{
			optionBackOut(inMap, inNavigate);
			if (!inMap && !inNavigate) //if map and navigation were closed in optionBackout
			{
				break; //Sentinel, breaks out of while loop to initiate close
			}
		}
		else
		{
			printf("Unknown command, ensure command is lowercase\n");
			printCommandList(); //Print command list
		}
    }
	free(userOption);
}

//Ends ros core services on both Remote PC and turtlebot3
void endRosServices(int socketDescriptor)
{
	char *protocolMessage=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store protocol from server
	protocolMessage[0] = '\0'; //Just in case, initialize string to be (effectively) empty
    char matchingString[MESSAGESIZE];

	if (send(socketDescriptor, "exit", MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
	{
		perror("Error sending message to server");
		
		close(socketDescriptor); //Close socket
	    free(protocolMessage); //Free memory allocated to store the incoming quot
		exit(-1);
	}
  
    strcpy(matchingString, "closing");
	if (recv(socketDescriptor, protocolMessage, MESSAGESIZE, 0) > 0) //If any message is sent by the server to the client
	{
		if (strcmp(protocolMessage, matchingString) != 0)
		{
			perror("Error recieving message from server");
			close(socketDescriptor); //Close socket
			free(protocolMessage); //Free memory allocated to store the incoming quote
			exit(-1);
		}
		printf("got closing\n");
		system("kill $(ps -aux | grep 'rqt_image_view' | grep -v grep | awk '{print $2}')");
		system("sleep 1");
		system("kill $(ps -aux | grep 'rosmaster' | grep -v grep | awk '{print $2}')");
		system("sleep 5");

	}
	else
	{
		perror("Error recieving message from server");
		
		close(socketDescriptor); //Close socket
		free(protocolMessage); //Free memory allocated to store the incoming quote
		exit(-1);
	}

	close(socketDescriptor); //Close socket
	free(protocolMessage); //Free memory allocated to store the incoming quote
    printf("All ros services started by this script have been stopped");
}
