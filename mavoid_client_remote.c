/*****************************************************************************/
/*  Author:            Bohdan D. Jacklitsch                                  */
/*  Creation Date:     11/4/2020                                             */
/*  Last Updated:      11/5/2020                                             */
/*  Due Date:          11/4/2020                                             */
/*  Course:            CSC328 - Network Programming                          */
/*  Professor Name:    Dr. Lisa Frye                                         */
/*  Assignment:        Assignment #8                                         */
/*  Filename:          qotd_BohdanJacklitsch.c                               */
/*  Language:          C                                                     */
/*  Compiler:          g++                                                   */
/*  Execution Command: ./a.out                                               */
/*  Purpose:           This program takes in arguments of a hostname of a    */
/*                     Quote of the Day server and protocol (TCP or UDP),    */
/*                     then acts as a client to said server in accordance to */
/*                     RFC 865 in the manner selected by the user (TCP/UDP). */
/*                                                                           */
/*  Notes:             Boy oh boy, two things really tripped me up. One was  */
/*                     needing double pointers to pass a pointer to a struct */
/*                     as I figured a pointer is passed by reference, but    */
/*                     apparently with only one it passed by value. The      */
/*                     other, and the reason this is a day late, is because  */
/*                     SOMEONE (not naming names) decided to stop reading    */
/*                     the RFC 865 protocol early, and as such did not       */
/*                     realize I - I mean that unnamed person - needed to    */
/*                     do a sendto before doing a recvfrom, just so the      */
/*                     server knows to send something. Makes a lot of sense  */
/*                     in hindsight, but took me around 4 hours to realize.  */
/*****************************************************************************/

//As usual, no clue what most of these do or if they are necessary
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

//Port number to connect to server,  as set forth by RFC 865
#define PORTNUMBER 62864
//Number of bytes to send to/receive from server
#define MESSAGESIZE 16

void interpretServerAddress(char *inputServer, struct sockaddr_in **serverAddress);

void sendIPAddress(int socketDescriptor, struct sockaddr_in **serverAddress, char* remoteIP);

int main(int argc, char **argv)
{
	int socketDescriptor; //Socket file descriptor for the client
	struct sockaddr_in *serverAddress; //Address for the server
	
	interpretServerAddress(argv[1], &serverAddress); //Convert the server address provided by the user to the correct server
	
	sendIPAddress(socketDescriptor, &serverAddress, argv[2]); //Send IP using TCP protocol
	
	close(socketDescriptor); //Close socket
	
	return 0;
}

void interpretServerAddress(char *inputServer, struct sockaddr_in **serverAddress)
{
	struct addrinfo *serverInfo = (struct addrinfo *)malloc(sizeof(struct addrinfo)); //Allocate memory for addrinfo for server details
	
	int returnHolder; //Stores return value of function/process for evaluation
	
	memset(serverInfo, 0, sizeof(serverInfo));
	serverInfo->ai_family = AF_INET; //As we are using an Internet Standard, seems reasonable that it would work with just the internet
	serverInfo->ai_flags = 0; //No flags that I am aware of
	serverInfo->ai_protocol = IPPROTO_TCP; //Might be redundant, but restricts protocol to only TCP.
	serverInfo->ai_socktype = SOCK_STREAM; //To make the socket TCP
	
	returnHolder = getaddrinfo(inputServer, 0, serverInfo, &serverInfo);
	if (returnHolder < 0) //If error
	{
		printf("Error in getaddrinfo call: %s\n", gai_strerror(returnHolder));
		exit(-1);
	}
	
	*serverAddress = (struct sockaddr_in *)serverInfo->ai_addr; //Extract address
	(*serverAddress)->sin_port = htons(PORTNUMBER); //Make Port number to RFC 865 standard
	
	free(serverInfo); //Free memory of addrinfo, as server info was interpreted
}

void sendIPAddress(int socketDescriptor, struct sockaddr_in **serverAddress, char* remoteIP)
{  
	char shutoffString[9] = "Received";
	int returnHolder; //Stores return value of function/process for evaluation
	
	socketDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); //Create a socket for the client
	if (socketDescriptor < 0)
	{
		perror("Error creating socket: ");
		exit(-1);
	}
	
	returnHolder = connect(socketDescriptor, (struct sockaddr *) *serverAddress, sizeof(struct sockaddr)); //Connect to server
	if (returnHolder == -1) //If error
	{
		perror("Error connecting to server: ");
		exit(-1);
	}
	
	if (send(socketDescriptor, remoteIP, MESSAGESIZE, 0) <= 0) //If message is not sent by the client to server
	{
		perror("Error sending message to server");
		
		close(socketDescriptor); //Close socket
		exit(-1);
	}
	
	char *confirmMessage=(char *)malloc(sizeof(char)*MESSAGESIZE); //Store confirmation message from server
	confirmMessage[0] = '\0'; //Just in case, initialize string to be (effectively) empty
  
	if (recv(socketDescriptor, confirmMessage, MESSAGESIZE, 0) > 0) //If any message is sent by the server to the client
	{
		if (strcmp(confirmMessage, shutoffString) != 0)
		{
			perror("Error recieving message from server");
			close(socketDescriptor); //Close socket
			free(confirmMessage); //Free memory allocated to store the incoming quote
			exit(-1);
		}
	}
	else
	{
		perror("Error recieving message from server");
		
		close(socketDescriptor); //Close socket
		free(confirmMessage); //Free memory allocated to store the incoming quote
		exit(-1);
	}
	close(socketDescriptor); //Close socket
	free(confirmMessage); //Free memory allocated to store the incoming quote
}
