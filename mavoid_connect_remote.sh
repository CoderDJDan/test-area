#!/bin/bash
remoteIP=$(ifconfig | grep 'inet ' | grep -v '127.0.0.1' | sed -n -e 's/^.*inet //p' | sed -n -e 's/\s.*//p')
sed -i "s/ROS_MASTER_URI=.*/ROS_MASTER_URI=http:\/\/$remoteIP:11311/" ~/.bashrc
sed -i "s/ROS_HOSTNAME=.*/ROS_HOSTNAME=$remoteIP/" ~/.bashrc
turtlebotIP=$(ping -c 1 KU-turtlebot3 | cut -d'(' -f 2 | cut -d')' -f 1 | head -n 1)
./a.out $turtlebotIP $remoteIP
